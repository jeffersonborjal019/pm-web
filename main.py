from fastapi import FastAPI, Request, Depends
from fastapi.responses import HTMLResponse
from fastapi.templating import Jinja2Templates
from fastapi.staticfiles import StaticFiles
from sqlalchemy.orm import Session
from routes import document_routes, user_routes, department_routes, login_routes, project_routes, task_routes, user_type_routes, job_routes, employee_routes, activity_routes, quotation_routes, history_routes, budget_requirements_routes, milestone_routes, stakeholder_routes, tor_routes, concept_paper_routes, vendor_routes
from database import get_db
from models import department_model, employee_model, job_model, user_model, project_model, quotation_model, task_model, activity_model, document_model, history_model, budget_requirements_model, milestone_model, stakeholders_model, tor_model, concept_paper_model, vendor_model
from models.user_model import User
from models.employee_model import Employee
from models.task_model import Task
from models.project_model import Project
from database import engine
from controllers.token_controller import get_token
from models.concept_paper_model import ConceptPaper

# Register template folder
template = Jinja2Templates('templates')

department_model.Base.metadata.create_all(engine)
user_model.Base.metadata.create_all(engine)
employee_model.Base.metadata.create_all(engine)
job_model.Base.metadata.create_all(engine)
project_model.Base.metadata.create_all(engine)
quotation_model.Base.metadata.create_all(engine)
task_model.Base.metadata.create_all(engine)
activity_model.Base.metadata.create_all(engine)
document_model.Base.metadata.create_all(engine)
history_model.Base.metadata.create_all(engine)
milestone_model.Base.metadata.create_all(engine)
budget_requirements_model.Base.metadata.create_all(engine)
stakeholders_model.Base.metadata.create_all(engine)
concept_paper_model.Base.metadata.create_all(engine)
tor_model.Base.metadata.create_all(engine)
vendor_model.Base.metadata.create_all(engine)

app = FastAPI()
# Mount static folder
app.mount('/static', StaticFiles(directory='static'), name='static')

# Register Routes
app.include_router(user_type_routes.router)
app.include_router(job_routes.router)
app.include_router(user_routes.router)
# app.include_router(user_profile_routes.router)
app.include_router(employee_routes.router)
app.include_router(department_routes.router)
app.include_router(concept_paper_routes.router)
app.include_router(project_routes.router)
app.include_router(document_routes.router)
app.include_router(task_routes.router)
app.include_router(activity_routes.router)
app.include_router(quotation_routes.router)
app.include_router(history_routes.router)
app.include_router(budget_requirements_routes.router)
app.include_router(milestone_routes.router)
app.include_router(stakeholder_routes.router)
app.include_router(tor_routes.router)
app.include_router(vendor_routes.router)
app.include_router(login_routes.router)
# app.include_router(admin_routes.router)
# app.include_router(manager_routes.router)

@app.get('/', response_class=HTMLResponse)
def home(request: Request, db: Session = Depends(get_db)):
    return template.TemplateResponse('index.html', {
        'request': request,
    })

# LOGIN PAGE
@app.get('/login', response_class=HTMLResponse)
def login(request: Request, db: Session = Depends(get_db)):
    users = db.query(User).all()
    return template.TemplateResponse('login.html', {
        'request': request,
        'users': users
    })

# ADMIN

# SYSADMIN PAGE
@app.get('/admin/sysadmin', response_class=HTMLResponse)
def sysadmin(request: Request, db: Session = Depends(get_db)):
    return template.TemplateResponse('admin/sysadmin.html', {
        'request': request,
    })

# JOB TITLES PAGE
@app.get('/admin/job_titles', response_class=HTMLResponse)
def user_types(request: Request, db: Session = Depends(get_db)):
    users = db.query(User).all()
    return template.TemplateResponse('admin/job_title.html', {
        'request': request,
        'users': users
    })

# USER TYPE PAGE
@app.get('/admin/user_type', response_class=HTMLResponse)
def user_types(request: Request, db: Session = Depends(get_db)):
    users = db.query(User).all()
    return template.TemplateResponse('admin/user_type.html', {
        'request': request,
        'users': users
    })

# USERS PAGE
@app.get('/admin/users', response_class=HTMLResponse)
def users(request: Request, db: Session = Depends(get_db)):
    users = db.query(User).all()
    return template.TemplateResponse('admin/users.html', {
        'request': request,
        'users': users
    })

# USER PROFILE PAGE
@app.get('/admin/user_profile', response_class=HTMLResponse)
def users(request: Request, db: Session = Depends(get_db)):
    users = db.query(User).all()
    return template.TemplateResponse('admin/user_profile.html', {
        'request': request,
        'users': users
    })

# EMPLOYEES
@app.get('/admin/employees', response_class=HTMLResponse)
def users(request: Request, db: Session = Depends(get_db)):
    users = db.query(User).all()
    return template.TemplateResponse('admin/employee.html', {
        'request': request,
        'users': users
    })

# DEPARTMENTS PAGE
@app.get('/admin/departments', response_class=HTMLResponse)
def departments(request: Request, db: Session = Depends(get_db)):
    users = db.query(User).all()
    return template.TemplateResponse('admin/departments.html', {
        'request': request,
        'users': users
    })

# MAIN DASHBOARD PAGE
@app.get('/admin/main_dashboard', response_class=HTMLResponse)
def main_dashboard(request: Request, db: Session = Depends(get_db)):
    return template.TemplateResponse('admin/main_dashboard.html', {
        'request': request,
    })

# DASHBOARD PAGE
@app.get('/admin/dashboard', response_class=HTMLResponse)
def dashboard(request: Request, db: Session = Depends(get_db)):
    users = db.query(User).all()
    data = db.query(Project).filter(Project.active_status == "Active", Project.approval_status == "Approved").all()
    tasks = db.query(Task).filter(Task.active_status == "Active").all()

    return template.TemplateResponse('admin/dashboard.html', {
        'request': request,
        'users': users,
        "data": data,
        "tasks": tasks
    })

# CONCEPT PAPER REQUESTS PAGE
@app.get('/admin/concept_paper_requisition', response_class=HTMLResponse)
def project_requisition(request: Request, db: Session = Depends(get_db)):
    return template.TemplateResponse('admin/concept_paper_requisition.html', {
        'request': request,
    })

# CONCEPT PAPER DETAILS PAGE
@app.get('/admin/concept_paper_details/{id}', response_class=HTMLResponse)
def project_requisition(id: str, request: Request, db: Session = Depends(get_db)):
    data = db.query(ConceptPaper).filter(ConceptPaper.id == id).first()
    return template.TemplateResponse('admin/concept_paper_details.html', {
        'request': request,
        'data': data
    })

# CALENDAR PAGE
@app.get('/admin/calendar', response_class=HTMLResponse)
def calendar(request: Request, db: Session = Depends(get_db)):
    users = db.query(User).all()
    return template.TemplateResponse('admin/calendar.html', {
        'request': request,
        'users': users
    })

# PROJECT REQUESTS PAGE
@app.get('/admin/project_requisition', response_class=HTMLResponse)
def project_requisition(request: Request, db: Session = Depends(get_db)):
    return template.TemplateResponse('admin/project_requisition.html', {
        'request': request,
    })

# PROJECT LISTS PAGE
@app.get('/admin/project_lists', response_class=HTMLResponse)
def project_lists(request: Request, db: Session = Depends(get_db)):
    return template.TemplateResponse('admin/project_lists.html', {
        'request': request,
    })

# CREATE PROJECT PAGE
@app.get('/admin/create_project', response_class=HTMLResponse)
def create_project(request: Request, db: Session = Depends(get_db)):
    return template.TemplateResponse('admin/create_project.html', {
        'request': request,
    })

# ALL PROJECT LISTS PAGE
@app.get('/admin/all_projects', response_class=HTMLResponse)
def all_projects_lists(request: Request, db: Session = Depends(get_db)):
    return template.TemplateResponse('admin/projects/all_projects.html', {
        'request': request,
    })

# ON PROGRESS PROJECT LISTS PAGE
@app.get('/admin/on_progress', response_class=HTMLResponse)
def on_progress_project_lists(request: Request, db: Session = Depends(get_db)):
    return template.TemplateResponse('admin/projects/on_progress_projects.html', {
        'request': request,
    })

# COMPLETED PROJECT LISTS PAGE
@app.get('/admin/completed', response_class=HTMLResponse)
def completed_project_lists(request: Request, db: Session = Depends(get_db)):
    return template.TemplateResponse('admin/projects/completed_projects.html', {
        'request': request,
    })

# CANCELLED PROJECT LISTS PAGE
@app.get('/admin/cancelled', response_class=HTMLResponse)
def cancelled_project_lists(request: Request, db: Session = Depends(get_db)):
    return template.TemplateResponse('admin/projects/cancelled_projects.html', {
        'request': request,
    })

# DELAYED PROJECT LISTS PAGE
@app.get('/admin/delayed', response_class=HTMLResponse)
def delayed_project_lists(request: Request, db: Session = Depends(get_db)):
    return template.TemplateResponse('admin/projects/delayed_projects.html', {
        'request': request,
    })

# ON HOLD PROJECT LISTS PAGE
@app.get('/admin/on_hold', response_class=HTMLResponse)
def on_hold_project_lists(request: Request, db: Session = Depends(get_db)):
    return template.TemplateResponse('admin/projects/on_hold_projects.html', {
        'request': request,
    })

# VIEW PROJECT PAGE
@app.get('/admin/view_project/{id}', response_class=HTMLResponse)
def view_one_project(id: str, request: Request, db: Session = Depends(get_db)):
    project = db.query(Project).filter(Project.id == id).first()
    return template.TemplateResponse('admin/view_project.html', {
        'request': request,
        'data': project
    })

# VIEW PROJECT DETAILS PAGE
@app.get('/admin/project_details/{id}', response_class=HTMLResponse)
def get_one_project(id: str, request: Request, db: Session = Depends(get_db)):
    project = db.query(Project).filter(Project.id == id).first()
    return template.TemplateResponse('admin/project_details.html', {
        'request': request,
        'data': project
    })

# ALL PROJECT QUOTATIONS PAGE
@app.get('/admin/project_quotations/{id}', response_class=HTMLResponse)
def view_project_quotation(id: str, request: Request, db: Session = Depends(get_db)):
    project = db.query(Project).filter(Project.id == id, Project.active_status == "Active").first()
    return template.TemplateResponse('admin/project_quotations.html', {
        'request': request,
        'data': project
    })

# ALL PROJECT TASKS PAGE
@app.get('/admin/all_project_task/{id}', response_class=HTMLResponse)
def view_all_project_task(id: str, request: Request, db: Session = Depends(get_db)):
    project = db.query(Project).filter(Project.id == id, Project.active_status == "Active").first()
    return template.TemplateResponse('admin/all_project_tasks.html', {
        'request': request,
        'data': project
    })

# TASKS LIST PAGE
@app.get('/admin/task_lists', response_class=HTMLResponse)
def task_lists(request: Request, db: Session = Depends(get_db)):
    return template.TemplateResponse('admin/task_lists.html', {
        'request': request,
    })

# TASK DETAILS PAGE
@app.get('/admin/task_details/{id}', response_class=HTMLResponse)
def get_one_task(id: str, request: Request, db: Session = Depends(get_db)):
    task = db.query(Task).filter(Task.id == id).first()
    return template.TemplateResponse('admin/task_details.html', {
        'request': request,
        'data': task
    })

# REPORTS PAGE
@app.get('/admin/reports', response_class=HTMLResponse)
def reports(request: Request, db: Session = Depends(get_db)):
    project = db.query(Project).filter(Project.active_status == "Active", Project.progress_status == "Completed").all()
    paper = db.query(ConceptPaper).filter(ConceptPaper.active_status == "Active", ConceptPaper.approval_status == "Approved").all()
    return template.TemplateResponse('admin/reports.html', {
        'request': request,
        'project': project,
        'paper': paper
    })

# TOR PAGE
@app.get('/admin/terms_of_reference', response_class=HTMLResponse)
def terms_of_reference(request: Request, db: Session = Depends(get_db)):
    return template.TemplateResponse('admin/terms_of_reference.html', {
        'request': request,
    })

# ARCHIVE PAGE
@app.get('/admin/archive', response_class=HTMLResponse)
def reports(request: Request, db: Session = Depends(get_db)):
    project = db.query(Project).filter(Project.active_status == "Active", Project.progress_status == "Completed").all()
    return template.TemplateResponse('admin/archive.html', {
        'request': request
    })

# DEPARTMENT HEAD

# MAIN DASHBOARD PAGE
@app.get('/department_head/main_dashboard', response_class=HTMLResponse)
def main_dashboard(request: Request, db: Session = Depends(get_db)):
    return template.TemplateResponse('department_head/main_dashboard.html', {
        'request': request,
    })

# DASHBOARD PAGE
@app.get('/department_head/dashboard', response_class=HTMLResponse)
def dashboard(request: Request, db: Session = Depends(get_db), current_user: User = Depends(get_token)):
    users = db.query(User).filter(User.email == current_user).first()
    employee = db.query(Employee).filter(Employee.user_id == users.id).first()
    project = db.query(Project).filter(Project.department_id == employee.department_id, Project.active_status == "Active", Project.approval_status == "Approved").all()
    tasks = []
    for i in range(len(project)):
        data = db.query(Task).filter(Task.active_status == "Active", Task.project_id == project[i].id).all()
        tasks.extend(data)
    return template.TemplateResponse('department_head/dashboard.html', {
        'request': request,
        'users': users,
        "data": project,
        "tasks": tasks
    })

# CONCEPT PAPER REQUESTS PAGE
@app.get('/department_head/concept_paper_requisition', response_class=HTMLResponse)
def project_requisition(request: Request, db: Session = Depends(get_db)):
    return template.TemplateResponse('department_head/concept_paper_requisition.html', {
        'request': request,
    })

# CONCEPT PAPER DETAILS PAGE
@app.get('/department_head/concept_paper_details/{id}', response_class=HTMLResponse)
def concept_paper_details(id: str, request: Request, db: Session = Depends(get_db)):
    data = db.query(ConceptPaper).filter(ConceptPaper.id == id).first()
    return template.TemplateResponse('department_head/concept_paper_details.html', {
        'request': request,
        'data': data,
    })

# CALENDAR PAGE
@app.get('/department_head/calendar', response_class=HTMLResponse)
def calendar(request: Request, db: Session = Depends(get_db)):
    users = db.query(User).all()
    return template.TemplateResponse('department_head/calendar.html', {
        'request': request,
        'users': users
    })

# PROJECT REQUESTS PAGE
@app.get('/department_head/project_requisition', response_class=HTMLResponse)
def project_requisition(request: Request, db: Session = Depends(get_db)):
    return template.TemplateResponse('department_head/project_requisition.html', {
        'request': request,
    })

# PROJECT LISTS PAGE
@app.get('/department_head/project_lists', response_class=HTMLResponse)
def project_lists(request: Request, db: Session = Depends(get_db)):
    return template.TemplateResponse('department_head/project_lists.html', {
        'request': request,
    })

# CREATE PROJECT PAGE
@app.get('/department_head/create_project', response_class=HTMLResponse)
def create_project(request: Request, db: Session = Depends(get_db)):
    return template.TemplateResponse('department_head/create_project.html', {
        'request': request,
    })

# ALL PROJECT LISTS PAGE
@app.get('/department_head/all_projects', response_class=HTMLResponse)
def all_projects_lists(request: Request, db: Session = Depends(get_db)):
    return template.TemplateResponse('department_head/projects/all_projects.html', {
        'request': request,
    })

# ON PROGRESS PROJECT LISTS PAGE
@app.get('/department_head/on_progress', response_class=HTMLResponse)
def on_progress_project_lists(request: Request, db: Session = Depends(get_db)):
    return template.TemplateResponse('department_head/projects/on_progress_projects.html', {
        'request': request,
    })

# COMPLETED PROJECT LISTS PAGE
@app.get('/department_head/completed', response_class=HTMLResponse)
def completed_project_lists(request: Request, db: Session = Depends(get_db)):
    return template.TemplateResponse('department_head/projects/completed_projects.html', {
        'request': request,
    })

# CANCELLED PROJECT LISTS PAGE
@app.get('/department_head/cancelled', response_class=HTMLResponse)
def cancelled_project_lists(request: Request, db: Session = Depends(get_db)):
    return template.TemplateResponse('department_head/projects/cancelled_projects.html', {
        'request': request,
    })

# DELAYED PROJECT LISTS PAGE
@app.get('/department_head/delayed', response_class=HTMLResponse)
def delayed_project_lists(request: Request, db: Session = Depends(get_db)):
    return template.TemplateResponse('department_head/projects/delayed_projects.html', {
        'request': request,
    })

# ON HOLD PROJECT LISTS PAGE
@app.get('/department_head/on_hold', response_class=HTMLResponse)
def on_hold_project_lists(request: Request, db: Session = Depends(get_db)):
    return template.TemplateResponse('department_head/projects/on_hold_projects.html', {
        'request': request,
    })

# VIEW PROJECT PAGE
@app.get('/department_head/view_project/{id}', response_class=HTMLResponse)
def view_one_project(id: str, request: Request, db: Session = Depends(get_db)):
    project = db.query(Project).filter(Project.id == id).first()
    return template.TemplateResponse('department_head/view_project.html', {
        'request': request,
        'data': project
    })

# VIEW PROJECT DETAILS PAGE
@app.get('/department_head/project_details/{id}', response_class=HTMLResponse)
def get_one_project(id: str, request: Request, db: Session = Depends(get_db)):
    project = db.query(Project).filter(Project.id == id).first()
    return template.TemplateResponse('department_head/project_details.html', {
        'request': request,
        'data': project
    })

# ALL PROJECT QUOTATIONS PAGE
@app.get('/department_head/project_quotations/{id}', response_class=HTMLResponse)
def view_project_quotation(id: str, request: Request, db: Session = Depends(get_db)):
    project = db.query(Project).filter(Project.id == id, Project.active_status == "Active").first()
    return template.TemplateResponse('department_head/project_quotations.html', {
        'request': request,
        'data': project
    })

# ALL PROJECT TASKS PAGE
@app.get('/department_head/all_project_task/{id}', response_class=HTMLResponse)
def view_all_project_task(id: str, request: Request, db: Session = Depends(get_db)):
    project = db.query(Project).filter(Project.id == id, Project.active_status == "Active").first()
    return template.TemplateResponse('department_head/all_project_tasks.html', {
        'request': request,
        'data': project
    })

# TASKS LIST PAGE
@app.get('/department_head/task_lists', response_class=HTMLResponse)
def task_lists(request: Request, db: Session = Depends(get_db)):
    return template.TemplateResponse('department_head/task_lists.html', {
        'request': request,
    })

# TASK DETAILS PAGE
@app.get('/department_head/task_details/{id}', response_class=HTMLResponse)
def get_one_task(id: str, request: Request, db: Session = Depends(get_db)):
    task = db.query(Task).filter(Task.id == id).first()
    return template.TemplateResponse('department_head/task_details.html', {
        'request': request,
        'data': task
    })

# REPORTS PAGE
@app.get('/department_head/reports', response_class=HTMLResponse)
def reports(request: Request, db: Session = Depends(get_db), current_user: User = Depends(get_token)):
    users = db.query(User).filter(User.email == current_user).first()
    employee = db.query(Employee).filter(Employee.user_id == users.id).first()
    data = db.query(Project).filter(Project.active_status == "Active", Project.progress_status == "Completed", Project.department_id == employee.department_id).all()
    paper = db.query(ConceptPaper).filter(ConceptPaper.active_status == "Active", ConceptPaper.approval_status == "Approved", ConceptPaper.department_id == employee.department_id).all()
    return template.TemplateResponse('department_head/reports.html', {
        'request': request,
        'project': data,
        'paper': paper
    })

# TOR PAGE
@app.get('/department_head/terms_of_reference', response_class=HTMLResponse)
def terms_of_reference(request: Request, db: Session = Depends(get_db)):
    return template.TemplateResponse('department_head/terms_of_reference.html', {
        'request': request,
    })

# ARCHIVE PAGE
# @app.get('/department_head/archive', response_class=HTMLResponse)
# def reports(request: Request, db: Session = Depends(get_db)):
#     project = db.query(Project).filter(Project.active_status == "Active", Project.progress_status == "Completed").all()
#     return template.TemplateResponse('department_head/archive.html', {
#         'request': request
#     })

# CONTACT LOGS PAGE
@app.get('/department_head/logs', response_class=HTMLResponse)
def logs(request: Request, db: Session = Depends(get_db)):
    users = db.query(User).all()
    data = db.query(User).filter(User.active_status == "Active").all()
    return template.TemplateResponse('department_head/logs.html', {
        'request': request,
        'users': users,
        "data": data,
    })


# MANAGER

# MAIN DASHBOARD PAGE
@app.get('/project_officer/main_dashboard', response_class=HTMLResponse)
def main_dashboard(request: Request, db: Session = Depends(get_db)):
    return template.TemplateResponse('users/main_dashboard.html', {
        'request': request,
    })

# DASHBOARD PAGE
@app.get('/project_officer/dashboard', response_class=HTMLResponse)
def dashboard(request: Request, db: Session = Depends(get_db)):
    return template.TemplateResponse('users/dashboard.html', {
        'request': request,
    })

# CONCEPT PAPER REQUESTS PAGE
@app.get('/project_officer/concept_paper_requisition', response_class=HTMLResponse)
def concept_paper_requisition(request: Request, db: Session = Depends(get_db)):
    return template.TemplateResponse('users/concept_paper_requisition.html', {
        'request': request,
    })

# CREATE CONCEPT PAPER PAGE
@app.get('/project_officer/create_concept_paper', response_class=HTMLResponse)
def create_project(request: Request, db: Session = Depends(get_db)):
    return template.TemplateResponse('users/create_concept_paper.html', {
        'request': request,
    })

# CONCEPT PAPER DETAILS PAGE
@app.get('/project_officer/concept_paper_details/{id}', response_class=HTMLResponse)
def concept_paper_details(id: str, request: Request, db: Session = Depends(get_db)):
    data = db.query(ConceptPaper).filter(ConceptPaper.id == id).first()
    return template.TemplateResponse('users/concept_paper_details.html', {
        'request': request,
        'data': data,
    })

# CALENDAR PAGE
@app.get('/project_officer/calendar', response_class=HTMLResponse)
def calendar(request: Request, db: Session = Depends(get_db)):
    users = db.query(User).all()
    return template.TemplateResponse('users/calendar.html', {
        'request': request,
        'users': users
    })

# PROJECT REQUESTS PAGE
@app.get('/project_officer/project_requisition', response_class=HTMLResponse)
def project_requisition(request: Request, db: Session = Depends(get_db)):
    return template.TemplateResponse('users/project_requisition.html', {
        'request': request,
    })

# PROJECT LISTS PAGE
@app.get('/project_officer/project_lists', response_class=HTMLResponse)
def project_lists(request: Request, db: Session = Depends(get_db)):
    return template.TemplateResponse('users/project_lists.html', {
        'request': request,
    })

# CREATE PROJECT PAGE
@app.get('/project_officer/create_project', response_class=HTMLResponse)
def create_project(request: Request, db: Session = Depends(get_db)):
    return template.TemplateResponse('users/create_project.html', {
        'request': request,
    })

# VIEW PROJECT PAGE
@app.get('/project_officer/view_project/{id}', response_class=HTMLResponse)
def view_one_project(id: str, request: Request, db: Session = Depends(get_db)):
    project = db.query(Project).filter(Project.id == id).first()
    return template.TemplateResponse('users/view_project.html', {
        'request': request,
        'data': project
    })

# VIEW PROJECT DETAILS PAGE
@app.get('/project_officer/project_details/{id}', response_class=HTMLResponse)
def get_one_project(id: str, request: Request, db: Session = Depends(get_db)):
    project = db.query(Project).filter(Project.id == id).first()
    return template.TemplateResponse('users/project_details.html', {
        'request': request,
        'data': project
    })

# ALL PROJECT QUOTATIONS PAGE
@app.get('/project_officer/project_quotations/{id}', response_class=HTMLResponse)
def view_all_project_task(id: str, request: Request, db: Session = Depends(get_db)):
    data = db.query(Project).filter(Project.id == id, Project.active_status == "Active").first()
    return template.TemplateResponse('users/project_quotations.html', {
        'request': request,
        'data': data
    })

# ALL PROJECT TASKS PAGE
@app.get('/project_officer/all_project_task/{id}', response_class=HTMLResponse)
def view_all_project_task(id: str, request: Request, db: Session = Depends(get_db)):
    data = db.query(Project).filter(Project.id == id, Project.active_status == "Active").first()
    return template.TemplateResponse('users/all_project_tasks.html', {
        'request': request,
        'data': data
    })

# TASKS LIST PAGE
@app.get('/project_officer/task_lists', response_class=HTMLResponse)
def task_lists(request: Request, db: Session = Depends(get_db)):
    return template.TemplateResponse('users/task_lists.html', {
        'request': request,
    })

# TASK DETAILS PAGE
@app.get('/project_officer/task_details/{id}', response_class=HTMLResponse)
def get_one_task(id: str, request: Request, db: Session = Depends(get_db)):
    task = db.query(Task).filter(Task.id == id).first()
    return template.TemplateResponse('users/task_details.html', {
        'request': request,
        'data': task
    })

# REPORTS PAGE
@app.get('/project_officer/reports', response_class=HTMLResponse)
def reports(request: Request, db: Session = Depends(get_db), current_user: User = Depends(get_token)):
    users = db.query(User).filter(User.email == current_user).first()
    employee = db.query(Employee).filter(Employee.user_id == users.id).first()
    data = db.query(Project).filter(Project.active_status == "Active", Project.progress_status == "Completed", Project.manager_id == employee.id).all()
    paper = db.query(ConceptPaper).filter(ConceptPaper.active_status == "Active", ConceptPaper.approval_status == "Approved", ConceptPaper.manager_id == employee.id).all()
    return template.TemplateResponse('users/reports.html', {
        'request': request,
        'project': data,
        'paper': paper
    })

# TOR PAGE
@app.get('/project_officer/terms_of_reference', response_class=HTMLResponse)
def terms_of_reference(request: Request, db: Session = Depends(get_db)):
    return template.TemplateResponse('users/terms_of_reference.html', {
        'request': request,
    })

# ERROR PAGE
@app.get('/error_page', response_class=HTMLResponse)
def project_lists(request: Request, db: Session = Depends(get_db)):
    return template.TemplateResponse('/error_page.html', {
        'request': request,
    })