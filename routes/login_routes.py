from fastapi import APIRouter, Depends, HTTPException, Response
from database import get_db
from models.user_model import User
from sqlalchemy.orm import Session, joinedload
from controllers import encryption, token_controller
from fastapi import APIRouter, Depends, HTTPException
from fastapi.security import OAuth2PasswordRequestForm
from sqlalchemy.orm import Session
from jose import jwt
from passlib.context import CryptContext

router = APIRouter(tags=['Authentication'])

secret = 'a very shady secret'
pwd_context = CryptContext(schemes=['bcrypt'], deprecated='auto')

def password_verify(plain, hashed):
    return pwd_context.verify(plain, hashed)

def password_hash(password):
    return pwd_context.hash(password)

# LOGIN
@router.post('/login')
async def login(response: Response, request: OAuth2PasswordRequestForm = Depends(), db: Session = Depends(get_db)):
    user = db.query(User).options(joinedload(User.user_type),
                    joinedload(User.user_type)).filter(User.email == request.username).first()
    if not user:
        return 404
    if not encryption.Hash.verify(user.password, request.password):
        return 402
    
    access_token = token_controller.create_access_token(data={"sub": user.email})
    response.set_cookie('token', access_token, httponly=True)
    return {
        "access_token": access_token, 
        "token_type": "bearer", 
        "data": user,
        }

# @router.post('/login')
# def verify(form: AdminAuthForm, response: Response, db: Session = Depends(get_db)):
#     try:
#         user_admin = db.query(User).filter(User.email == form.email).first()
#         if not user_admin:
#             return 404
#         elif user_admin:
#             match = password_verify(form.password, user_admin.password)
#             if not match:
#                 return 402
#             elif match:
#                 data = AdminTokenData(
#                     admin_email = user_admin.email
#                 )
#                 token = jwt.encode(dict(data), secret)
#                 response.set_cookie('token', token, httponly=True)
#                 return {
#                     'data' : data,
#                     'token' : token,
#                     'message': 'Login Successfully'
#                 }

#         return {'message': 'User not found.'}
#     except Exception as e:
#         print(e)

# LOG OUT
@router.post('/logout')
async def logout(response: Response):
    response.delete_cookie('token')
    return {'message': 'Logout Success!'}
