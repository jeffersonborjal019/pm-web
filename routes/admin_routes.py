from os import name
from fastapi import APIRouter, Request, Depends, HTTPException, Form
from sqlalchemy.orm import Session
from models.project_model import Project
from database import get_db
from datetime import datetime as dt
from fastapi.responses import HTMLResponse
from fastapi.templating import Jinja2Templates
from models.user_model import User
from models.task_model import Task
from controllers.token_controller import get_token

router = APIRouter(
    prefix='/admin',
    tags=['admin'],
    dependencies=[Depends(get_token)]
)

template = Jinja2Templates('templates')

# DASHBOARD PAGE
@router.get('/dashboard', response_class=HTMLResponse)
def dashboard(request: Request, db: Session = Depends(get_db)):
    users = db.query(User).all()
    data = db.query(Project).filter(Project.active_status == "Active", Project.approval_status == "Approved").all()
    tasks = db.query(Task).filter(Task.active_status == "Active").all()

    return template.TemplateResponse('admin/dashboard.html', {
        'request': request,
        'users': users,
        "data": data,
        "tasks": tasks
    })

# CALENDAR PAGE
@router.get('/calendar', response_class=HTMLResponse)
def calendar(request: Request, db: Session = Depends(get_db)):
    users = db.query(User).all()
    return template.TemplateResponse('admin/calendar.html', {
        'request': request,
        'users': users
    })

# PROJECT REQUESTS PAGE
@router.get('/project_requisition', response_class=HTMLResponse)
def project_requisition(request: Request, db: Session = Depends(get_db)):
    return template.TemplateResponse('admin/project_requisition.html', {
        'request': request,
    })

# PROJECT LISTS PAGE
@router.get('/project_lists', response_class=HTMLResponse)
def project_lists(request: Request, db: Session = Depends(get_db)):
    return template.TemplateResponse('admin/project_lists.html', {
        'request': request,
    })

# ALL PROJECT LISTS PAGE
@router.get('/all_projects', response_class=HTMLResponse)
def all_projects_lists(request: Request, db: Session = Depends(get_db)):
    return template.TemplateResponse('admin/projects/all_projects.html', {
        'request': request,
    })

# ON PROGRESS PROJECT LISTS PAGE
@router.get('/on_progress', response_class=HTMLResponse)
def on_progress_project_lists(request: Request, db: Session = Depends(get_db)):
    return template.TemplateResponse('admin/projects/on_progress_projects.html', {
        'request': request,
    })

# COMPLETED PROJECT LISTS PAGE
@router.get('/completed', response_class=HTMLResponse)
def completed_project_lists(request: Request, db: Session = Depends(get_db)):
    return template.TemplateResponse('admin/projects/completed_projects.html', {
        'request': request,
    })

# CANCELLED PROJECT LISTS PAGE
@router.get('/cancelled', response_class=HTMLResponse)
def cancelled_project_lists(request: Request, db: Session = Depends(get_db)):
    return template.TemplateResponse('admin/projects/cancelled_projects.html', {
        'request': request,
    })

# DELAYED PROJECT LISTS PAGE
@router.get('/delayed', response_class=HTMLResponse)
def delayed_project_lists(request: Request, db: Session = Depends(get_db)):
    return template.TemplateResponse('admin/projects/delayed_projects.html', {
        'request': request,
    })

# ON HOLD PROJECT LISTS PAGE
@router.get('/on_hold', response_class=HTMLResponse)
def on_hold_project_lists(request: Request, db: Session = Depends(get_db)):
    return template.TemplateResponse('admin/projects/on_hold_projects.html', {
        'request': request,
    })

# VIEW PROJECT PAGE
@router.get('/view_project/{id}', response_class=HTMLResponse)
def view_one_project(id: str, request: Request, db: Session = Depends(get_db)):
    project = db.query(Project).filter(Project.id == id).first()
    return template.TemplateResponse('admin/view_project.html', {
        'request': request,
        'data': project
    })

# VIEW PROJECT DETAILS PAGE
@router.get('/project_details/{id}', response_class=HTMLResponse)
def get_one_project(id: str, request: Request, db: Session = Depends(get_db)):
    project = db.query(Project).filter(Project.id == id).first()
    return template.TemplateResponse('admin/project_details.html', {
        'request': request,
        'data': project
    })

# ALL PROJECT TASKS PAGE
@router.get('/all_project_task/{id}', response_class=HTMLResponse)
def view_all_project_task(id: str, request: Request, db: Session = Depends(get_db)):
    project = db.query(Project).filter(Project.id == id, Project.active_status == "Active").first()
    return template.TemplateResponse('admin/all_project_tasks.html', {
        'request': request,
        'data': project
    })

# TASKS LIST PAGE
@router.get('/task_lists', response_class=HTMLResponse)
def task_lists(request: Request, db: Session = Depends(get_db)):
    return template.TemplateResponse('admin/task_lists.html', {
        'request': request,
    })

# TASK DETAILS PAGE
@router.get('/task_details/{id}', response_class=HTMLResponse)
def get_one_task(id: str, request: Request, db: Session = Depends(get_db)):
    task = db.query(Task).filter(Task.id == id).first()
    return template.TemplateResponse('admin/task_details.html', {
        'request': request,
        'data': task
    })

# REPORTS PAGE
@router.get('/reports', response_class=HTMLResponse)
def reports(request: Request, db: Session = Depends(get_db)):
    project = db.query(Project).filter(Project.active_status == "Active", Project.progress_status == "Completed").all()
    return template.TemplateResponse('admin/reports.html', {
        'request': request,
        'project': project
    })

# ARCHIVE PAGE
@router.get('/archive', response_class=HTMLResponse)
def reports(request: Request, db: Session = Depends(get_db)):
    project = db.query(Project).filter(Project.active_status == "Active", Project.progress_status == "Completed").all()
    return template.TemplateResponse('admin/archive.html', {
        'request': request
    })

# CONTACT LOGS PAGE
@router.get('/logs', response_class=HTMLResponse)
def logs(request: Request, db: Session = Depends(get_db)):
    users = db.query(User).all()
    data = db.query(User).filter(User.active_status == "Active").all()
    return template.TemplateResponse('admin/logs.html', {
        'request': request,
        'users': users,
        "data": data,
    })

# JOB TITLES PAGE
@router.get('/job_titles', response_class=HTMLResponse)
def user_types(request: Request, db: Session = Depends(get_db)):
    users = db.query(User).all()
    return template.TemplateResponse('admin/job_title.html', {
        'request': request,
        'users': users
    })

# USER TYPE PAGE
@router.get('/user_type', response_class=HTMLResponse)
def user_types(request: Request, db: Session = Depends(get_db)):
    users = db.query(User).all()
    return template.TemplateResponse('admin/user_type.html', {
        'request': request,
        'users': users
    })

# USERS PAGE
@router.get('/users', response_class=HTMLResponse)
def users(request: Request, db: Session = Depends(get_db)):
    users = db.query(User).all()
    return template.TemplateResponse('admin/users.html', {
        'request': request,
        'users': users
    })

# USER PROFILE PAGE
@router.get('/user_profile', response_class=HTMLResponse)
def users(request: Request, db: Session = Depends(get_db)):
    users = db.query(User).all()
    return template.TemplateResponse('admin/user_profile.html', {
        'request': request,
        'users': users
    })

# EMPLOYEES
@router.get('/employees', response_class=HTMLResponse)
def users(request: Request, db: Session = Depends(get_db)):
    users = db.query(User).all()
    return template.TemplateResponse('admin/employee.html', {
        'request': request,
        'users': users
    })

# DEPARTMENTS PAGE
@router.get('/departments', response_class=HTMLResponse)
def departments(request: Request, db: Session = Depends(get_db)):
    users = db.query(User).all()
    return template.TemplateResponse('admin/departments.html', {
        'request': request,
        'users': users
    })