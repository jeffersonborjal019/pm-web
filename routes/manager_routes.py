from os import name
from fastapi import APIRouter, Request, Depends, HTTPException, Form
from sqlalchemy.orm import Session
from models.employee_model import Employee
from models.project_model import Project
from database import get_db
from datetime import datetime as dt
from fastapi.responses import HTMLResponse
from fastapi.templating import Jinja2Templates
from models.user_model import User
from models.task_model import Task
from models.department_model import Department
from controllers.token_controller import get_token

router = APIRouter(
    prefix='/manager',
    tags=['manager'],
    dependencies=[Depends(get_token)]
)

template = Jinja2Templates('templates')

# DASHBOARD PAGE
@router.get('/dashboard', response_class=HTMLResponse)
def dashboard(request: Request, db: Session = Depends(get_db)):
    return template.TemplateResponse('users/dashboard.html', {
        'request': request,
    })

# CALENDAR PAGE
@router.get('/calendar', response_class=HTMLResponse)
def calendar(request: Request, db: Session = Depends(get_db)):
    users = db.query(User).all()
    return template.TemplateResponse('users/calendar.html', {
        'request': request,
        'users': users
    })

# PROJECT REQUESTS PAGE
@router.get('/project_requisition', response_class=HTMLResponse)
def project_requisition(request: Request, db: Session = Depends(get_db)):
    return template.TemplateResponse('users/project_requisition.html', {
        'request': request,
    })

# PROJECT LISTS PAGE
@router.get('/project_lists', response_class=HTMLResponse)
def project_lists(request: Request, db: Session = Depends(get_db)):
    return template.TemplateResponse('users/project_lists.html', {
        'request': request,
    })

# VIEW PROJECT PAGE
@router.get('/view_project/{id}', response_class=HTMLResponse)
def view_one_project(id: str, request: Request, db: Session = Depends(get_db)):
    project = db.query(Project).filter(Project.id == id).first()
    return template.TemplateResponse('users/view_project.html', {
        'request': request,
        'data': project
    })

# VIEW PROJECT DETAILS PAGE
@router.get('/project_details/{id}', response_class=HTMLResponse)
def get_one_project(id: str, request: Request, db: Session = Depends(get_db)):
    project = db.query(Project).filter(Project.id == id).first()
    return template.TemplateResponse('users/project_details.html', {
        'request': request,
        'data': project
    })

# ALL PROJECT TASKS PAGE
@router.get('/all_project_task/{id}', response_class=HTMLResponse)
def view_all_project_task(id: str, request: Request, db: Session = Depends(get_db)):
    data = db.query(Project).filter(Project.id == id, Project.active_status == "Active").first()
    return template.TemplateResponse('users/all_project_tasks.html', {
        'request': request,
        'data': data
    })

# TASKS LIST PAGE
@router.get('/task_lists', response_class=HTMLResponse)
def task_lists(request: Request, db: Session = Depends(get_db)):
    return template.TemplateResponse('users/task_lists.html', {
        'request': request,
    })

# TASK DETAILS PAGE
@router.get('/task_details/{id}', response_class=HTMLResponse)
def get_one_task(id: str, request: Request, db: Session = Depends(get_db)):
    task = db.query(Task).filter(Task.id == id).first()
    return template.TemplateResponse('users/task_details.html', {
        'request': request,
        'data': task
    })

# REPORTS PAGE
@router.get('/reports', response_class=HTMLResponse)
def reports(request: Request, db: Session = Depends(get_db), current_user: User = Depends(get_token)):
    users = db.query(User).filter(User.email == current_user).first()
    employee = db.query(Employee).filter(Employee.user_id == users.id).first()
    data = db.query(Project).filter(Project.active_status == "Active", Project.progress_status == "Completed", Project.manager_id == employee.id).all()
    project = db.query(Project).filter(Project.active_status == "Active", Project.progress_status == "Completed").all()
    return template.TemplateResponse('users/reports.html', {
        'request': request,
        'users': users,
        'employee': employee,
        'project': data
    })