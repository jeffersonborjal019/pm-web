from os import stat
from fastapi import APIRouter, Depends, Request, HTTPException, UploadFile, Form, File, status
from sqlalchemy.orm import Session
from sqlalchemy import or_
from typing import Optional
from schemas.employee_schema import ShowEmployee
from models.employee_model import Employee
from database import get_db
from typing import List
from controllers.encryption import Hash
from controllers.token_controller import get_token
from datetime import datetime as dt

router = APIRouter(
    prefix='/employee',
    tags=['employee']
)

# GET ALL EMPLOYEES
@router.get('/', status_code=status.HTTP_200_OK, response_model=List[ShowEmployee])
async def all_employee(db: Session = Depends(get_db)):
    employee = db.query(Employee).filter(Employee.active_status == "Active").all()
    return employee

# GET ONE EMPLOYEE
@router.get('/{id}', status_code=status.HTTP_200_OK, response_model=ShowEmployee)
async def get_one_employee(id: str, db: Session = Depends(get_db)):
    employee = db.query(Employee).filter(Employee.id == id).first()
    if not employee:
        raise HTTPException(404, 'Employee not found')
    return employee

# GET ONE EMPLOYEE LOGIN
@router.get('/user/{user_id}', status_code=status.HTTP_200_OK, response_model=ShowEmployee)
async def get_one_employee(user_id: str, db: Session = Depends(get_db)):
    employee = db.query(Employee).filter(Employee.user_id == user_id).first()
    if not employee:
        raise HTTPException(404, 'Employee not found')
    return employee

# GET ALL DEPARTMENT EMPLOYEES
@router.get('/department/{id}', status_code=status.HTTP_200_OK, response_model=List[ShowEmployee])
async def get_one_department_employee(id: str, db: Session = Depends(get_db)):
    employee = db.query(Employee).filter(Employee.active_status == 'Active', Employee.department_id == id).all()
    if not employee:
        raise HTTPException(404, 'Employee not found')
    return employee

# CREATE EMPLOYEES
@router.post('/', status_code=status.HTTP_201_CREATED)
async def create_employee(first_name: str = Form(...),
                            middle_name: Optional[str] = Form(None),
                            last_name: str = Form(...),
                            suffix_name: Optional[str] = Form(None),
                            user_id: str = Form(...), 
                            job_id: str = Form(...), 
                            department_id: str = Form(...), 
                            db: Session = Depends(get_db)):
    
    to_store = Employee(
        first_name = first_name,
        middle_name = middle_name,
        last_name = last_name,
        suffix_name = suffix_name,
        user_id = user_id,
        job_id = job_id,
        department_id = department_id
    )

    db.add(to_store)
    db.commit()
    return {'message': 'Employee stored successfully.'}

# UPDATE EMPLOYEE
@router.put('/{id}', status_code=status.HTTP_202_ACCEPTED)
async def update_employee(id: str,
                            first_name: str = Form(...),
                            middle_name: Optional[str] = Form(None),
                            last_name: str = Form(...),
                            suffix_name: Optional[str] = Form(None),
                            user_id: str = Form(...), 
                            job_id: str = Form(...), 
                            department_id: str = Form(...), 
                            db: Session = Depends(get_db)): 
    if not db.query(Employee).filter(Employee.id == id).update({
        'first_name': first_name,
        'middle_name': middle_name,
        'last_name': last_name,
        'suffix_name': suffix_name,
        'user_id': user_id,
        'job_id': job_id,
        'department_id': department_id,
        'updated_at' : dt.utcnow()
    }):
        raise HTTPException(404, 'Employee to update is not found')
    db.commit()
    return {'message': 'Employee updated successfully.'}

# DELETE EMPLOYEE
@router.delete('/{id}', status_code=status.HTTP_204_NO_CONTENT)
async def delete_employee(id: str, db: Session = Depends(get_db)):
    if not db.query(Employee).filter(Employee.id == id).update({
        'active_status': "Inactive"
    }):
        raise HTTPException(404, 'Employee to delete is not found')
    db.commit()
    return {'message': 'Employee deleted successfully.'}

