from os import stat
from fastapi import APIRouter, Depends, Request, HTTPException, UploadFile, Form, File, status
from sqlalchemy.orm import Session
from sqlalchemy import or_
from schemas.user_type_schema import ShowUserType
from models.user_model import UserType
from database import get_db
from typing import List
from controllers.encryption import Hash
from controllers.token_controller import get_token
from datetime import datetime as dt

router = APIRouter(
    prefix='/user_types',
    tags=['user_types']
)

# GET ALL USER TYPES
@router.get('/', status_code=status.HTTP_200_OK)
async def all_user_types(db: Session = Depends(get_db)):
    user_types = db.query(UserType).filter(UserType.active_status == "Active").all()
    return user_types

# GET ONE USER TYPE
@router.get('/{id}', status_code=status.HTTP_200_OK, response_model=ShowUserType)
async def get_one_user_type(id: str, db: Session = Depends(get_db)):
    user_type = db.query(UserType).filter(UserType.id == id).first()
    if not user_type:
        raise HTTPException(404, 'User Type not found')
    return user_type

# CREATE USER TYPE
@router.post('/', status_code=status.HTTP_201_CREATED)
async def create_user_type(title: str = Form(...), db: Session = Depends(get_db)):
    to_store = UserType(
        title = title
    )

    db.add(to_store)
    db.commit()
    return {'message': 'User Type stored successfully.'}

# UPDATE USER TYPE
@router.put('/{id}', status_code=status.HTTP_202_ACCEPTED)
async def update_user_type(id: str, title: str = Form(...), db: Session = Depends(get_db)): 
    if not db.query(UserType).filter(UserType.id == id).update({
        'title': title,
        'updated_at' : dt.utcnow()
    }):
        raise HTTPException(404, 'User Type to update is not found')
    db.commit()
    return {'message': 'User Type updated successfully.'}

# DELETE USER TYPE
@router.delete('/{id}', status_code=status.HTTP_204_NO_CONTENT)
async def delete_user(id: str, db: Session = Depends(get_db)):
    if not db.query(UserType).filter(UserType.id == id).update({
        'active_status': "Inactive"
    }):
        raise HTTPException(404, 'User Type to delete is not found')
    db.commit()
    return {'message': 'User Type deleted successfully.'}

