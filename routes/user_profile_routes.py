from os import stat
from fastapi import APIRouter, Depends, Request, HTTPException, UploadFile, Form, File, status
from fastapi_utils.guid_type import GUID
from sqlalchemy.orm import Session
from sqlalchemy import or_
from typing import Optional
from schemas.user_profile_schema import ShowUserProfile
from models.user_model import UserType, UserProfile
from models.department_model import Department
from database import get_db
from typing import List
from controllers.encryption import Hash
from controllers.token_controller import get_token
from datetime import datetime as dt

router = APIRouter(
    prefix='/user_profile',
    tags=['user_profile']
)

# GET ALL USER PROFILE
@router.get('/', status_code=status.HTTP_200_OK, response_model=List[ShowUserProfile])
async def all_user_profile(db: Session = Depends(get_db)):
    user_profile = db.query(UserProfile).filter(UserProfile.active_status == "Active").all()
    return user_profile

# GET ONE USER PROFILE
@router.get('/{id}', status_code=status.HTTP_200_OK, response_model=ShowUserProfile)
async def get_one_user_profile(id: str, db: Session = Depends(get_db)):
    user_profile = db.query(UserProfile).filter(UserProfile.id == id).first()
    if not user_profile:
        raise HTTPException(404, 'User Profile not found')
    return user_profile

# GET ONE USER PROFILE LOGIN
@router.get('/user/{user_id}', status_code=status.HTTP_200_OK, response_model=ShowUserProfile)
async def get_one_user_profile_login(user_id: str, db: Session = Depends(get_db)):
    user_profile = db.query(UserProfile).filter(UserProfile.user_id == user_id).first()
    if not user_profile:
        raise HTTPException(404, 'User Profile not found')
    return user_profile

# CREATE USER PROFILE
@router.post('/', status_code=status.HTTP_201_CREATED)
async def create_user_profile(first_name: str = Form(...), middle_name: str = Form(None), last_name: str = Form(...), suffix_name: Optional[str] = Form(None), birthdate: str = Form(...), gender: str = Form(...), house_number: str = Form(...), street: str = Form(...), barangay: str = Form(...), municipality: str = Form(...), province: str = Form(...), user_id: str = Form(...), db: Session = Depends(get_db)):
    # file_location = f"documents/{document.filename}"
    # with open(file_location, "wb") as buffer:
    #     shutil.copyfileobj(document.file, buffer)

    # document = document.filename

    to_store = UserProfile(
        first_name = first_name,
        middle_name = middle_name,
        last_name = last_name,
        suffix_name = suffix_name,
        birthdate = birthdate,
        gender = gender,
        house_number = house_number,
        street = street,
        barangay = barangay,
        municipality = municipality,
        province = province,
        user_id = user_id
    )

    db.add(to_store)
    db.commit()
    return {'message': 'User Profile stored successfully.'}

# UPDATE USER PROFILE
@router.put('/{id}', status_code=status.HTTP_202_ACCEPTED)
async def update_user_profile(id: str, first_name: str = Form(...), middle_name: str = Form(None), last_name: str = Form(...), suffix_name: str = Form(None), birthdate: str = Form(...), gender: str = Form(...), house_number: str = Form(...), street: str = Form(...), barangay: str = Form(...), municipality: str = Form(...), province: str = Form(...), user_id: str = Form(...), db: Session = Depends(get_db)): 
    if not db.query(UserProfile).filter(UserProfile.id == id).update({
        'first_name': first_name,
        'middle_name': middle_name,
        'last_name': last_name,
        'suffix_name': suffix_name,
        'birthdate': birthdate,
        'gender': gender,
        'house_number': house_number,
        'street': street,
        'barangay': barangay,
        'municipality': municipality,
        'province': province,
        'user_id': user_id,
        'updated_at' : dt.utcnow()
    }):
        raise HTTPException(404, 'User Profile to update is not found')
    db.commit()
    return {'message': 'User Profile updated successfully.'}

# DELETE USER PROFILE
@router.delete('/{id}', status_code=status.HTTP_204_NO_CONTENT)
async def delete_user(id: str, db: Session = Depends(get_db)):
    if not db.query(UserProfile).filter(UserProfile.id == id).update({
        'active_status': "Inactive"
    }):
        raise HTTPException(404, 'User Profile to delete is not found')
    db.commit()
    return {'message': 'User Profile deleted successfully.'}

