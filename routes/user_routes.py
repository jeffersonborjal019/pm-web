from os import stat
from schemas.employee_schema import ShowEmployee
from fastapi import APIRouter, Depends, Request, HTTPException, UploadFile, Form, File, status
from sqlalchemy.orm import Session
from sqlalchemy import or_
from schemas.user_schema import ShowUser
from schemas.employee_schema import ShowEmployee
from models.user_model import User
from models.employee_model import Employee
from models.department_model import Department
from database import get_db
from datatables import DataTable
from typing import List
from controllers.encryption import Hash
from controllers.token_controller import get_token
from datetime import datetime as dt

router = APIRouter(
    prefix='/users',
    tags=['users']
)

# USERS DATATABLE
@router.get('/datatable', status_code=status.HTTP_200_OK)
async def datatable(request: Request, db: Session = Depends(get_db)):
    try:
        def perform_search(queryset, user_input):
            return queryset.filter(
                or_(
                    Department.name.like('%' + user_input + '%'),
                    User.first_name.like('%' + user_input + '%'),
                    User.last_name.like('%' + user_input + '%'),
                    User.email.like('%' + user_input + '%'),
                    User.type.like('%' + user_input + '%')
                )
            )

        table = DataTable(dict(request.query_params), User, db.query(User).filter(User.active_status == "Active"), [
            'id',
            ('department_name', 'departments.name'),
            'first_name',
            'last_name',
            'email',
            'type',
            'created_at',
            'active_status'
        ])

        table.searchable(lambda queryset, user_input: perform_search(queryset, user_input))
    
        return table.json()
    except Exception as e:
        print(e)

# GET ALL USERS
@router.get('/', status_code=status.HTTP_200_OK, response_model=List[ShowUser])
async def all_users(db: Session = Depends(get_db)):
    users = db.query(User).filter(User.active_status == "Active").all()
    return users

# GET ONE USER
@router.get('/{id}', status_code=status.HTTP_200_OK, response_model=ShowUser)
async def get_one_user(id: str, db: Session = Depends(get_db)):
    user = db.query(User).filter(User.id == id).first()
    if not user:
        raise HTTPException(404, 'User not found')
    return user

# GET ALL DEPARTMENT USER
@router.get('/department/{department_id}', status_code=status.HTTP_200_OK, response_model = List[ShowEmployee])
async def get_all_user_department(department_id: str, db: Session = Depends(get_db)):
    user = db.query(Employee).filter(Employee.job.has(title="Project Officer"), Employee.department_id == department_id).all()
    if not user:
        raise HTTPException(404, 'No Project Officer')
    return user

# CREATE USER
@router.post('/', status_code=status.HTTP_201_CREATED)
async def create_user(email: str = Form(...), password: str = Form(...), user_type_id: str = Form(...), db: Session = Depends(get_db)):
    to_store = User(
        email = email,
        password = Hash.bcrypt(password),
        user_type_id = user_type_id
    )

    db.add(to_store)
    db.commit()
    return {'message': 'User stored successfully.'}

# UPDATE USER
@router.put('/{id}', status_code=status.HTTP_202_ACCEPTED)
async def update_user(id: str, email: str = Form(...), user_type_id: str = Form(...), db: Session = Depends(get_db)): 
    if not db.query(User).filter(User.id == id).update({
        'email': email,
        'user_type_id': user_type_id,
        'updated_at' : dt.utcnow()
    }):
        raise HTTPException(404, 'User to update is not found')
    db.commit()
    return {'message': 'User updated successfully.'}

# DELETE USER
@router.delete('/{id}', status_code=status.HTTP_204_NO_CONTENT)
async def delete_user(id: str, db: Session = Depends(get_db)):
    if not db.query(User).filter(User.id == id).update({
        'active_status': "Inactive"
    }):
        raise HTTPException(404, 'User to delete is not found')
    db.commit()
    return {'message': 'User deleted successfully.'}

