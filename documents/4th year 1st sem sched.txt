Monday
2:00PM 	- 5:00PM	Buhay at Mga Sinulat ni Rizal
5:00PM 	- 7:00PM	IAS 2
7:00PM 	- 9:00PM 	Social and Professional Issues in IT

Tuesday
10:30AM - 1:30PM	Science, Technology and Society
2:00PM	- 5:00PM	Elective 3
5:00PM 	- 7:00PM	Elective 4
7:00PM	- 9:00PM	Elective 4

Wednesday
5:00PM 	- 8:00PM	Capstone 2

Thursday
12:30PM	- 2:30PM	Elective 3
5:00PM	- 6:00PM	Elective 4

Friday
6:00PM 	- 7:00PM	IAS 2
8:00PM	- 9:00PM	Social and Professional Issues in IT

Saturday
7:30AM - 12:30AM 	Systems Administration and Maintenance
