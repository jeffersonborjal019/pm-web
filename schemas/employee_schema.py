from datetime import datetime as dt
from schemas.department_schema import ShowDepartment
from pydantic import BaseModel
from schemas.job_schema import ShowJob
from schemas.user_schema import ShowUser

class EmployeeBase(BaseModel):
    first_name : str
    middle_name : str
    last_name : str
    suffix_name : str
    user_id: str
    job_id: str
    department_id: str
    class Config():
        orm_mode = True

# Schema for request body
class CreateEmployee(EmployeeBase):
    pass

class ShowEmployee(EmployeeBase):
    id: str
    job: ShowJob
    departments: ShowDepartment
    user_employee: ShowUser
    active_status: str
    created_at: dt
    class Config():
        orm_mode = True

# Schema for response body
class Employee(BaseModel):
    created_at: dt
    updated_at: dt