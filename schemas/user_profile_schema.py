from datetime import datetime as dt
from pydantic import BaseModel
from schemas.user_schema import ShowUser
from typing import Optional

class UserProfileBase(BaseModel):
    first_name: str
    middle_name: Optional[str]
    last_name: str
    suffix_name: Optional[str]
    birthdate: dt
    gender: str
    house_number: str
    street: str
    barangay: str
    municipality: str
    province: str
    user_id: str
    class Config():
        orm_mode = True

# Schema for request body
class CreateUserProfile(UserProfileBase):
    pass

class ShowUserProfile(UserProfileBase):
    id: str
    user: ShowUser
    active_status: str
    created_at: dt
    class Config():
        orm_mode = True

# Schema for response body
class UserProfile(BaseModel):
    created_at: dt
    updated_at: dt