from datetime import datetime as dt
from pydantic import BaseModel

class UserTypeBase(BaseModel):
    title: str
    class Config():
        orm_mode = True

# Schema for request body
class CreateUserType(UserTypeBase):
    pass

class ShowUserType(UserTypeBase):
    id: str
    active_status: str
    created_at: dt
    class Config():
        orm_mode = True

# Schema for response body
class UserType(BaseModel):
    created_at: dt
    updated_at: dt