from datetime import datetime as dt
from pydantic import BaseModel

class ProjectTeamBase(BaseModel):
    name: str
    project_id: str
    employee_id: str
    class Config():
        orm_mode = True

# Schema for request body
class CreateProjectTeam(ProjectTeamBase):
    pass

class ShowProjectTeam(ProjectTeamBase):
    id: str
    active_status: str
    created_at: dt
    class Config():
        orm_mode = True

# Schema for response body
class ProjectTeam(BaseModel):
    created_at: dt
    updated_at: dt