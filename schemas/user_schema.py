from datetime import datetime as dt
from pydantic import BaseModel
from schemas.user_type_schema import ShowUserType
from typing import Optional

class UserBase(BaseModel):
    email: str
    password: Optional[str]
    user_type_id: str
    class Config():
        orm_mode = True

# Schema for request body
class CreateUser(UserBase):
    pass

class ShowUser(UserBase):
    id: str
    user_type: ShowUserType
    active_status: str
    created_at: dt
    class Config():
        orm_mode = True

class ShowManager(UserBase):
    id: str
    email: str
    user_type: ShowUserType
    class Config():
        orm_mode: True

# Schema for response body
class User(BaseModel):
    created_at: dt
    updated_at: dt